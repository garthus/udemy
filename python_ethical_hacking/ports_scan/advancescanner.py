#!/usr/bin/python3

from socket import *
import optparse
from threading import *

def main():
    parser = optparse.OptionParser('How to use: ' + ' -H <target host> -p <target ports>')
    parser.add_option('-H', dest='tgtHost', type='string', help='specify target host')
    parser.add_option('-p', dest='tgtPorts', type='string', help='specify target Ports separated by comma')
    (options, args) = parser.parse_args()
    tgtHost = options.tgtHost
    tgtPorts = str(options.tgtPorts).split(',')
    if (tgtHost == None) | (tgtPorts[0] == None) :
        print(parser.usage)
        exit(0)
    #portScan(tgtHost,tgtPorts)

main()
