#!/usr/bin/python3

import socket
from termcolor import colored

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

host = input("Wich host will be scanned ? : ")

def portscanner(port):
        if sock.connect_ex((host,port)):
                print(colored("port %d is closed" %(port), 'red'))
        else:
                print(colored("port %d is opened" %(port), 'yellow'))

for port in range(20,30):
    portscanner(port)
