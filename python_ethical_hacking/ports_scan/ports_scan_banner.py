#!/bin/python3

import socket

def retBanner(ip,port):
    try:
            socket.setdefaulttimeout(2)
            s = socket.socket()
            s.connect_ex((ip,port))
            banner = s.recv(1024).decode("Utf8")
            return str(banner)
    except:
            return

def main():
        ip = input("[*] Enter Target IP: ")
        for port in range(20,25):
            banner = retBanner(ip,port)
            if banner:
                print ("[+]" + ip + "/" + str(port) + " : " + banner.rstrip("\n"))
main()
